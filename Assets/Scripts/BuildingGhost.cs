using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGhost : MonoBehaviour
{
    public static BuildingGhost Instance { get; private set; }

    [SerializeField] LayerMask groundMask;
    private GameObject modelGameObject;
    private ResourceNearbyOverlay resourceNearbyOverlay;

    private void Awake()
    {
        Instance = this;
        modelGameObject = transform.Find("model").gameObject;
        resourceNearbyOverlay = transform.Find("pfResourceNearbyOverlay").GetComponent<ResourceNearbyOverlay>();
        Hide();
    }

    private void Start()
    {
        BuildingManager.Instance.OnActiveBuildingTypeChanged += BuildingManager_OnActiveBuildingTypeChanged;
    }

    private void BuildingManager_OnActiveBuildingTypeChanged(BuildingTypeSO activeBuildingType)
    {
        if (activeBuildingType == null)
        {
            Hide();
            resourceNearbyOverlay.Hide();
        }
        else
        {
            Show(activeBuildingType.mesh);
            if (activeBuildingType.hasResourceGeneratorData)
                resourceNearbyOverlay.Show(activeBuildingType.resourceGeneratorData);
            else
                resourceNearbyOverlay.Hide();
        }

    }

    private void Update()
    {
        transform.position = UtilsClass.GetMouseWorldPosition3D();
    }

    private void Show(Mesh ghostMesh)
    {
        modelGameObject.SetActive(true);
        modelGameObject.GetComponent<MeshFilter>().mesh = ghostMesh;
    }

    private void Hide()
    {
        modelGameObject.SetActive(false);
    }

    public void BuildingGhostMaterialColor(Color color)
    {
        modelGameObject.GetComponent<MeshRenderer>().material.color = color;
    }
}
