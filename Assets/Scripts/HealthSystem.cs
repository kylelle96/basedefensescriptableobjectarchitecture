using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthSystem : MonoBehaviour
{
    public UnityEvent OnDamaged;
    public UnityEvent OnDied;
    public UnityEvent OnHealed;

    [SerializeField] IntVariable _healthAmount;
    [SerializeField] IntReference _healthAmountMax;


    private void Awake()
    {
        //SetHealthAmountMax(_healthAmountMax, true);
        _healthAmount.Value = _healthAmountMax.Value;
    }



    public void Damage(int damageAmount)
    {
        _healthAmount.ApplyChange(-damageAmount);

        if(_healthAmount.Value <= 0)
        {
            _healthAmount.SetValue(0);
        }

        OnDamaged?.Invoke();

        if (isDead())
        {
            OnDied?.Invoke();
        }

    }

    public void Heal(int healAmount)
    {
        _healthAmount.ApplyChange(healAmount);
        if(_healthAmount.Value > _healthAmountMax.Value)
        {
            _healthAmount.SetValue(_healthAmountMax.Value);
        }

        OnHealed?.Invoke();

    }

    public void HealFull()
    {
        _healthAmount.SetValue(_healthAmountMax.Value);

        OnHealed?.Invoke();
    }

    public bool isDead()
    {
        return _healthAmount.Value == 0;
    }

    public int GetHealthAmount()
    {
        return _healthAmount.Value;
    }

    public int GetHealthAmountMax()
    {
        return _healthAmountMax.Value;
    }

    //public void SetHealthAmountMax(int healthAmountMax, bool updateHealthAmount)
    //{
    //     _healthAmountMax = healthAmountMax;
    //    if (updateHealthAmount)
    //    {
    //        _healthAmount = healthAmountMax;
    //    }
    //}

    public bool IsFullHealth()
    {
        return _healthAmount.Value == _healthAmountMax.Value;
    }

    //public float GetHealthAmountNormalize()
    //{
    //    return (float)_healthAmount.Value/ _healthAmountMax.Value;
    //}
}
