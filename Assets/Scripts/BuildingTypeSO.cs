using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/BuildingType")]
public class BuildingTypeSO : ScriptableObject
{
    public string nameString;
    public Transform prefab;
    public Sprite sprite;
    public Mesh mesh;
    public float minConstructionRadius;
    [Tooltip("Detecting Enemies (For Towers)")] 
    public float detectionRadius; //For towers
    [Tooltip("HQ and Light Tower recharge radius")]
    public float batteryChargeRadius; //For HQ and Light Tower
    public float constructionTimerMax;
    public int healthAmountMax;
    public bool hasResourceGeneratorData;
    public bool canRecharge;
    public ResourceGeneratorData resourceGeneratorData;
    public ResourceAmount[] constructionResourceCostArray;


    public string GetConstructionResourceCostString()
    {
        string str = "";
        foreach (ResourceAmount resourceAmount in constructionResourceCostArray)
        {
            str += "<color=#" + resourceAmount.resourceType.colorHex + ">" + resourceAmount.resourceType.nameShort + ": " + resourceAmount.amount + "</color>" + " ";
        }
        return str;
    }
}
