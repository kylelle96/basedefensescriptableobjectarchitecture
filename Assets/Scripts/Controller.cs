using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour, IDamagable
{
    public static Controller Instance { get; private set; }

    [SerializeField] float moveSpeed = 6f;

    Rigidbody rb;
    HealthSystem healthSystem;
    Camera viewCamera;
    Vector3 velocity;
    private bool isDead = false;

    private void Awake()
    {
        Instance = this;
        rb = GetComponent<Rigidbody>();
        healthSystem = GetComponent<HealthSystem>();
        //healthSystem.OnDamaged += HealthSystem_OnDamaged;
        //healthSystem.OnDied += HealthSystem_OnDied;
        viewCamera = Camera.main;
    }


    private void HealthSystem_OnDied()
    {
        SoundManager.Instance.PlaySound(SoundManager.Sound.GameOver);
        EnemyWaveUI.Instance.Hide();
        GameOverUI.Instance.Show();
        isDead = true;
    }

    private void HealthSystem_OnDamaged()
    {
        SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingDamaged); // TODO: Change to proper damage sound
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead) return;
        Vector3 mousePosition = viewCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));
        transform.LookAt(mousePosition + Vector3.up * transform.position.y);
        
    }

    private void FixedUpdate()
    {
        velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * moveSpeed;
        rb.MovePosition(rb.position + velocity * Time.deltaTime);
    }
}
