using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/EnemyType")]
public class EnemyTypeSO : ScriptableObject
{
    public string nameString;
    public Transform prefab;
    public int healthAmountMax;
    public float moveSpeed;
    public float detectionRadius;
    public int damage;
}
