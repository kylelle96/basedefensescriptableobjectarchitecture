using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour, IWeapon
{
    WeaponTypeSO weaponType;
    WeaponController weaponController;
    private float shootTimer;
    private float shootTimerMax;

    private void Awake()
    {
        weaponType = GetComponent<WeaponTypeHolder>().weaponType;
        shootTimerMax = weaponType.shootTimerMax;
        weaponController = transform.parent.GetComponent<WeaponController>();
        
    }

    private void Start()
    {
        weaponController.OnUpdateWeapon += WeaponController_OnUpdateWeapon;
    }

    private void OnDestroy()
    {
        weaponController.OnUpdateWeapon -= WeaponController_OnUpdateWeapon;
    }

    private void WeaponController_OnUpdateWeapon()
    {
        if (weaponController.GetActiveWeaponType() == weaponType)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Update()
    {
        shootTimer -= Time.deltaTime;
        if (Input.GetMouseButton(0) && BuildingManager.Instance.GetActiveBuilingType() == null)
        {
            if (shootTimer < 0)
            {
                Shoot();
                shootTimer = shootTimerMax;
            }
        }
       
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Shoot()
    {
        weaponType.projectilePrefab.Get<Projectile>(transform.position, transform.rotation);
    }

}
