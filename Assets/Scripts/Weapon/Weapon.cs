using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    WeaponTypeSO weaponType;
    WeaponController weaponController;
    private float shootTimer;
    private float shootTimerMax;


    private void Awake()
    {
        weaponType = GetComponent<WeaponTypeHolder>().weaponType;
        shootTimerMax = weaponType.shootTimerMax;
        weaponController = transform.parent.GetComponent<WeaponController>();

    }
}
