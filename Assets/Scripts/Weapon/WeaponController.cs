using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public event Action OnUpdateWeapon;

    private WeaponTypeSO activeWeaponType;

    private void Start()
    {
        WeaponManager.Instance.OnWeaponChanged += WeaponManager_OnWeaponChanged;
        UpdateWeapon();
    }

    private void OnDestroy()
    {
        WeaponManager.Instance.OnWeaponChanged -= WeaponManager_OnWeaponChanged;
    }

    private void WeaponManager_OnWeaponChanged()
    {
        UpdateWeapon();
    }

    private void UpdateWeapon()
    {
        activeWeaponType = WeaponManager.Instance.ActiveWeaponType;
        OnUpdateWeapon?.Invoke();
    }

    public WeaponTypeSO GetActiveWeaponType()
    {
        return activeWeaponType;
    }

}
