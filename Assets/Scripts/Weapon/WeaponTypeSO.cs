using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/WeaponType")]
public class WeaponTypeSO : ScriptableObject
{

    public string nameString;
    public Transform weaponPrefab;
    public Projectile projectilePrefab;
    public float shootTimerMax;
    public int damage;
    public float projectileSpeed;
    public float projectileTimerMax;

}
