using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingConstruction : MonoBehaviour
{
    public static BuildingConstruction Create(Vector3 position, BuildingTypeSO buildingType)
    {
        Transform pfBuildingConstruction = Resources.Load<Transform>("pfBuildingConstruction");
        Transform buildingConstructionTransform = Instantiate(pfBuildingConstruction, position, Quaternion.identity);

        BuildingConstruction buildingConstruction = buildingConstructionTransform.GetComponent<BuildingConstruction>();
        buildingConstruction.SetBuildingType(buildingType);

        return buildingConstruction;
    }

    private BuildingTypeSO buildingType;
    private BuildingTypeHolder buildingTypeHolder;
    private Transform buildingModel;
    private float constructionTimer;
    private float constructionTimerMax;
    private Material constructionMaterial;

    private void Awake()
    {
        buildingModel = transform.Find("model");
        buildingTypeHolder = GetComponent<BuildingTypeHolder>();
        constructionMaterial = buildingModel.GetComponent<MeshRenderer>().material;
       
    }

    private void Update()
    {
        constructionTimer -= Time.deltaTime;
        constructionMaterial.SetFloat("_Progress", GetConstructionTimerNormalize());
        if(constructionTimer <= 0f)
        {
            Debug.Log("Construction Complete!");
            Instantiate(buildingType.prefab, transform.position, Quaternion.identity);
            SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingPlaced);
            Destroy(gameObject);
        }
    }

    private void SetBuildingType(BuildingTypeSO buildingType)
    {
        this.constructionTimerMax = buildingType.constructionTimerMax;
        this.buildingType = buildingType;
        buildingTypeHolder.buildingType = buildingType;
        constructionTimer = constructionTimerMax;
        transform.GetComponent<BoxCollider>().size = buildingType.prefab.transform.Find("model").localScale;
        buildingModel.GetComponent<MeshFilter>().mesh = buildingType.mesh;
        buildingModel.Find("mask").GetComponent<MeshFilter>().mesh = buildingType.mesh;
        buildingModel.localScale = buildingType.prefab.transform.Find("model").localScale;
    }

    public float GetConstructionTimerNormalize()
    {
        return 1 - constructionTimer / constructionTimerMax;
    }
}
