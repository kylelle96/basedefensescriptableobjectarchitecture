using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    [SerializeField] IntVariable _playerHealthAmount;
    [SerializeField] IntReference _playerHealthAmountMax;

    private RectTransform barRectTransform;

    private void Awake()
    {
        barRectTransform = transform.Find("bar").GetComponent<RectTransform>();
    }

    private void Start()
    {
        UpdateBar();
    }

    private void HealthSystem_OnHealed()
    {
        UpdateBar();
    }

    private void HealthSystem_OnDamaged()
    {
        UpdateBar();
    }

    public void UpdateBar()
    {
        barRectTransform.localScale = new Vector3((float)_playerHealthAmount.Value / _playerHealthAmountMax.Value, 1, 1);
    }
}
