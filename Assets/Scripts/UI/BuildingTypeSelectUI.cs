using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingTypeSelectUI : MonoBehaviour
{
    [SerializeField] private Sprite arrowSprite;
    [SerializeField] private List<BuildingTypeSO> ignoreBuildingTypeList;

    private BuildingTypeListSO buildingTypeList;

    private Dictionary<BuildingTypeSO, Transform> buttonTypeTransformDictionary;
    private Transform arrowButton;

    private Animator anim;
    private bool isSelected = false;

    private void Awake()
    {
        Transform buttonTemplate = transform.Find("buttonTemplate");
        buttonTemplate.gameObject.SetActive(false);
        buttonTypeTransformDictionary = new Dictionary<BuildingTypeSO, Transform>();

        buildingTypeList = Resources.Load<BuildingTypeListSO>(typeof(BuildingTypeListSO).Name);

        anim = GetComponent<Animator>();

        arrowButton = Instantiate(buttonTemplate, transform);
        arrowButton.gameObject.SetActive(true);

        float offsetY = 80f;
        arrowButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -545);
        arrowButton.Find("image").GetComponent<Image>().sprite = arrowSprite;
        arrowButton.Find("text").GetComponent<TextMeshProUGUI>().SetText("Cursor");


        arrowButton.GetComponent<Button>().onClick.AddListener(() => {
            BuildingManager.Instance.SetActiveBuildingType(null);
        });

        MouseEnterExitEvents mouseEnterExitEvents = arrowButton.GetComponent<MouseEnterExitEvents>();
        mouseEnterExitEvents.OnMouseEnter += () => {
            TooltipUI.Instance.Show("Arrow");
        };

        mouseEnterExitEvents.OnMouseExit += () => {
            TooltipUI.Instance.Hide();
        };

        //index++;
        int index = 0;
        foreach (BuildingTypeSO buildingType in buildingTypeList.list)
        {
            if (ignoreBuildingTypeList.Contains(buildingType)) continue;

            Transform buttonTransform = Instantiate(buttonTemplate, transform);
            buttonTransform.gameObject.SetActive(true);

            buttonTransform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -offsetY * index);
            buttonTransform.Find("image").GetComponent<Image>().sprite = buildingType.sprite;
            buttonTransform.Find("text").GetComponent<TextMeshProUGUI>().SetText(buildingType.nameString);

            
            buttonTransform.GetComponent<Button>().onClick.AddListener(() => {
                BuildingManager.Instance.SetActiveBuildingType(buildingType);
            });

            mouseEnterExitEvents = buttonTransform.GetComponent<MouseEnterExitEvents>();
            mouseEnterExitEvents.OnMouseEnter += () => { TooltipUI.Instance.Show(buildingType.nameString + "\n" + buildingType.GetConstructionResourceCostString()); 
            };
            mouseEnterExitEvents.OnMouseExit += () => {
                TooltipUI.Instance.Hide();
            };

            buttonTypeTransformDictionary[buildingType] = buttonTransform;
            index++;
        }
    }

    private void Start()
    {
        BuildingManager.Instance.OnActiveBuildingTypeChanged += BuildingManager_OnActiveBuildingTypeChanged;
        BuildingManager.Instance.OnShowBuildingSelectedUI += BuildingManager_OnShowBuildingSelectedUI;
        UpdateActiveBuildingTypeButton();
        isSelected = true;
        ToggleBuildingSelectedUI();
    }

    private void OnDestroy()
    {
        MouseEnterExitEvents mouseEnterExitEvents = arrowButton.GetComponent<MouseEnterExitEvents>();
        mouseEnterExitEvents.OnMouseEnter -= () => { };
        mouseEnterExitEvents.OnMouseExit -= () => { };

        //Unsubscribe to OnMouseEnter and Exit Events when destroyed
        foreach (BuildingTypeSO buildingType in buildingTypeList.list)
        {
            if (ignoreBuildingTypeList.Contains(buildingType)) continue;

            mouseEnterExitEvents = buttonTypeTransformDictionary[buildingType].transform.GetComponent<MouseEnterExitEvents>();
            mouseEnterExitEvents.OnMouseEnter -= () => { };
            mouseEnterExitEvents.OnMouseExit -= () => { };
        }
    }

    private void BuildingManager_OnShowBuildingSelectedUI()
    {
        ToggleBuildingSelectedUI();
    }

    private void ToggleBuildingSelectedUI()
    {
        //gameObject.SetActive(!gameObject.activeSelf);
        isSelected = !isSelected;

        if (!isSelected)
        {
            BuildingManager.Instance.SetActiveBuildingType(null);
            anim.SetBool("isSelected", false);
        }
        else
        {
            anim.SetBool("isSelected", true);
        }
    }

    private void BuildingManager_OnActiveBuildingTypeChanged(BuildingTypeSO activeBuildingType)
    {
        UpdateActiveBuildingTypeButton();
    }


    private void UpdateActiveBuildingTypeButton()
    {
        arrowButton.Find("selected").gameObject.SetActive(false);

        foreach(BuildingTypeSO buildingType in buttonTypeTransformDictionary.Keys)
        {
            Transform buttonTransform = buttonTypeTransformDictionary[buildingType];
            buttonTransform.Find("selected").gameObject.SetActive(false);
        }

        BuildingTypeSO activeBuildingType = BuildingManager.Instance.GetActiveBuilingType();
        if(activeBuildingType == null)
        {
            arrowButton.Find("selected").gameObject.SetActive(true);
        }
        else
        {
            buttonTypeTransformDictionary[activeBuildingType].Find("selected").gameObject.SetActive(true);
        }
        
    }
}
